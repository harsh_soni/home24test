package com.harsh.articlestest.interfaces;

import android.view.View;

/**
 * Created by Harshsoni on 14/10/15.
 */
public interface LikeDislikeCallBack {
    public void onLikeCliked(View v, int pos);
    public void onDisLikeCliked(View v, int pos);
}

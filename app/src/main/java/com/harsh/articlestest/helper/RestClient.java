package com.harsh.articlestest.helper;

import com.squareup.okhttp.OkHttpClient;

import retrofit.RestAdapter;
import retrofit.client.OkClient;

public class RestClient {

  private static Api REST_CLIENT;
  private static String ROOT =
          "https://api-mobile.home24.com/api/v1";

  static {
    setupRestClient();
  }

  private RestClient() {}

  public static Api get() {
    return REST_CLIENT;
  }

  private static void setupRestClient() {
    RestAdapter.Builder builder = new RestAdapter.Builder()
     .setEndpoint(ROOT)
     .setClient(new OkClient(new OkHttpClient()));
      builder.setLogLevel(RestAdapter.LogLevel.FULL);
        
     RestAdapter restAdapter = builder.build();
     REST_CLIENT = restAdapter.create(Api.class);
  }
}
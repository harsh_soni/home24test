package com.harsh.articlestest.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.harsh.articlestest.R;
import com.harsh.articlestest.VO.ArticlesVO;
import com.harsh.articlestest.VO.MediaVO;
import com.harsh.articlestest.activities.ArticleSelectionActivity;
import com.harsh.articlestest.interfaces.LikeDislikeCallBack;
import com.squareup.picasso.Picasso;

import butterknife.Bind;
import butterknife.ButterKnife;


public class FragmentArticle extends Fragment {

	private FragmentActivity fragmentActivity;
    @Bind(R.id.imgArticle) ImageView imgArticle;
    @Bind(R.id.btnLike)
    Button btnLike;
    @Bind(R.id.btnDislike) Button btnDislike;

    private final String PNG = "image/png";
    private final String JPG = "image/jpg";
    private final String JPEG = "image/jpeg";
    private ArticleSelectionActivity activity;


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_article, container, false);
        ButterKnife.bind(this, view);

        activity = (ArticleSelectionActivity) getActivity();

		Bundle b = getArguments();
		ArticlesVO vo = b.getParcelable("data");
        MediaVO mediaVO = vo.getMedia().get(0);
        final int pos = b.getInt("pos");
        String url = mediaVO.getUri();
        String mime = mediaVO.getMimeType();

        if(mime.equalsIgnoreCase(PNG)){
            url = url+".png";
        }else if(mime.equalsIgnoreCase(JPG) | mime.equalsIgnoreCase(JPEG))  {
            url = url+".jpg";
        }else{
            url = url+".png";
        }

		Picasso.with(activity)
				.load(url)
				.placeholder(R.drawable.app_homelogo_small)
				.error(R.drawable.no_image)
				.into(imgArticle);

        btnLike.setSelected(false);
        btnDislike.setSelected(false);
        btnLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                btnDislike.setEnabled(true);
                btnLike.setEnabled(false);
                btnLike.setSelected(true);
                btnDislike.setSelected(false);

                LikeDislikeCallBack callback = activity;
                callback.onLikeCliked(v, pos);
            }
        });

        btnDislike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                btnDislike.setEnabled(false);
                btnLike.setEnabled(true);
                btnDislike.setSelected(true);
                btnLike.setSelected(false);

                LikeDislikeCallBack callback = activity;
                callback.onDisLikeCliked(v, pos);
            }
        });
		return view;
	}

	@Override
	public void onStart() {
		super.onStart();
	}

	public FragmentActivity getFragmentActivity() {
		return fragmentActivity;
	}

	public void setFragmentActivity(FragmentActivity fragmentActivity) {
		this.fragmentActivity = fragmentActivity;
	}
	
	
}

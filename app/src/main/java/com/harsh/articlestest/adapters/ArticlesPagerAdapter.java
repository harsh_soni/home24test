package com.harsh.articlestest.adapters;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.harsh.articlestest.VO.ArticlesVO;
import com.harsh.articlestest.fragments.FragmentArticle;
import com.harsh.articlestest.fragments.FragmentLast;

import java.util.ArrayList;

public class ArticlesPagerAdapter extends FragmentPagerAdapter {

	private ArrayList<ArticlesVO> arrArti;

	public ArticlesPagerAdapter(FragmentManager fm, ArrayList<ArticlesVO> arr) {
		super(fm);
		this.arrArti = arr;
	}

	@Override
	public Fragment getItem(int position) {

		if(position<getCount()-1){
			FragmentArticle f = new FragmentArticle();
			Bundle b = new Bundle();
			b.putInt("pos", position);
			b.putParcelable("data", arrArti.get(position));
			f.setArguments(b);
			return f;
		}else{
			Fragment f = new FragmentLast();
			return f;
		}
	}

	@Override
	public int getItemPosition(Object item) {
		return POSITION_NONE;
	}

	@Override
	public int getCount() {
        return arrArti.size()+1;
	}

}
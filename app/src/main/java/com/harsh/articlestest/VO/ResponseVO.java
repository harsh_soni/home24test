package com.harsh.articlestest.VO;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Harshsoni on 13/10/15.
 */
public class ResponseVO {

    private long articlesCount;
    @SerializedName("_embedded")
    private EmbeddedVO embeddedVO;

    public long getArticlesCount() {
        return articlesCount;
    }

    public void setArticlesCount(long articlesCount) {
        this.articlesCount = articlesCount;
    }

    public EmbeddedVO getEmbeddedVO() {
        return embeddedVO;
    }

    public void setEmbeddedVO(EmbeddedVO embeddedVO) {
        this.embeddedVO = embeddedVO;
    }
}

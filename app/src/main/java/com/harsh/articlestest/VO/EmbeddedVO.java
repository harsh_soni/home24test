package com.harsh.articlestest.VO;

import java.util.ArrayList;

/**
 * Created by Harshsoni on 13/10/15.
 */
public class EmbeddedVO {

    private ArrayList<ArticlesVO> articles;

    public ArrayList<ArticlesVO> getArticles() {
        return articles;
    }

    public void setArticles(ArrayList<ArticlesVO> articles) {
        this.articles = articles;
    }
}

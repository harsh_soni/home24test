package com.harsh.articlestest.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.harsh.articlestest.R;
import com.harsh.articlestest.VO.ArticlesVO;
import com.harsh.articlestest.adapters.ArticlesPagerAdapter;
import com.harsh.articlestest.customViews.NonSwipeableViewPager;
import com.harsh.articlestest.interfaces.LikeDislikeCallBack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ArticleSelectionActivity extends BaseActivity implements LikeDislikeCallBack{

    public @Bind(R.id.pager)
    NonSwipeableViewPager pager;
    public Menu optionMenu;
    public HashMap<Integer, Boolean> likeMap;
    ArrayList<ArticlesVO> arrArticles;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article_selection);

        ButterKnife.bind(this);

        setToolBar("Articles");
        likeMap = new HashMap<Integer, Boolean>();
        arrArticles = getIntent().getParcelableArrayListExtra("articles");

        pager.setAdapter(new ArticlesPagerAdapter(getSupportFragmentManager(), arrArticles));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_article_selection, menu);

        MenuItem itemReview = menu.findItem(R.id.action_review);
        LinearLayout ll = (LinearLayout) itemReview.getActionView();
        TextView tvReview = (TextView) ll.findViewById(R.id.tvReviewMenuitem);

        tvReview.setOnClickListener(reviewClick);
        optionMenu = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_review) {
            return true;
        }else if(id == R.id.action_count){
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        optionMenu = menu;
        int likeCount = 0;
        Set<Integer> keys = likeMap.keySet();
        if(keys!=null){
            for (Integer k : keys){
                if(likeMap.get(k)){
                    likeCount++;
                }
            }
        }


        MenuItem itemCount = menu.findItem(R.id.action_count);
        itemCount.setTitle(likeCount + "/"+arrArticles.size());

        MenuItem itemReview = menu.findItem(R.id.action_review);
        LinearLayout ll = (LinearLayout) itemReview.getActionView();
        TextView tvReview = (TextView) ll.findViewById(R.id.tvReviewMenuitem);

        if(pager.getCurrentItem()== pager.getAdapter().getCount()-1){
            itemReview.setEnabled(true);
            tvReview.setTextColor(Color.parseColor("#000000"));
        }else{
            itemReview.setEnabled(false);
            tvReview.setTextColor(Color.parseColor("#cccccc"));
        }




//        itemReview.setTitle(s);

        return super.onPrepareOptionsMenu(menu);
    }

    View.OnClickListener reviewClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            Intent i = new Intent(ArticleSelectionActivity.this, ReviewActivity.class);
            i.putParcelableArrayListExtra("articles", arrArticles);
            startActivity(i);

        }
    };

    @Override
    public void onLikeCliked(View v, int pos) {
        arrArticles.get(pos).setIsLiked(true);

        likeMap.put(pos, true);
        invalidateOptionsMenu();

        if(pos < pager.getAdapter().getCount()-1)
            pager.setCurrentItem(pager.getCurrentItem()+1);

    }

    @Override
    public void onDisLikeCliked(View v, int pos) {
        arrArticles.get(pos).setIsLiked(false);

        likeMap.put(pos,false);
        invalidateOptionsMenu();

        if(pos < pager.getAdapter().getCount()-1)
            pager.setCurrentItem(pager.getCurrentItem()+1);
    }
}

package com.harsh.articlestest.helper;


import com.harsh.articlestest.VO.ResponseVO;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

public interface Api {


    @GET("/articles?appDomain=1")
    void getArticles(@Query("limit") String limit, Callback<ResponseVO> callback);
//
//    @GET("/getImagesByCatId")
//    void getImagesByCatId(@Query("id") String catId, Callback<CategoryDataVO> callback);



}
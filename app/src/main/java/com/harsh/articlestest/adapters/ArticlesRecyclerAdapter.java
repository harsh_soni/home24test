package com.harsh.articlestest.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.harsh.articlestest.R;
import com.harsh.articlestest.VO.ArticlesVO;
import com.harsh.articlestest.VO.MediaVO;
import com.harsh.articlestest.activities.ReviewActivity;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ArticlesRecyclerAdapter extends RecyclerView.Adapter<ArticlesRecyclerAdapter.ProductRowHolder> {

        private List<ArticlesVO> productList;

        private final String PNG = "image/png";
        private final String JPG = "image/jpg";
        private final String JPEG = "image/jpeg";
        private Context mContext;

        public ArticlesRecyclerAdapter(Context context, List<ArticlesVO> feedItemList) {
            this.productList = feedItemList;
            this.mContext = context;
        }

        @Override
        public ProductRowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
//            View v = LayoutInflater.from(viewGroup.getContext()).inflate(isProductViewAsList ? R.layout.row_review_articles : R.layout.product_row_layout_grid, null);
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_review_articles, null);
            ProductRowHolder mh = new ProductRowHolder(v);
            return mh;
        }

        @Override
        public void onBindViewHolder(ProductRowHolder  productRowHolder, int i) {
            ArticlesVO item = productList.get(i);

            MediaVO mediaVO = item.getMedia().get(0);
            String url = mediaVO.getUri();
            String mime = mediaVO.getMimeType();

            if(mime.equalsIgnoreCase(PNG)){
                url = url+".png";
            }else if(mime.equalsIgnoreCase(JPG) | mime.equalsIgnoreCase(JPEG))  {
                url = url+".jpg";
            }else{
                url = url+".png";
            }

            Picasso.with(mContext)
                    .load(url)
                    .placeholder(R.drawable.app_homelogo_small).resize(500,500)
                    .error(R.drawable.no_image)
                    .into(productRowHolder.imageView);

            if(ReviewActivity.isProductViewAsList){
                productRowHolder.textView.setBackgroundColor(Color.parseColor("#99000000"));
                productRowHolder.textView.setText(item.getTitle());
            }else{
                productRowHolder.textView.setBackgroundColor(Color.TRANSPARENT);
                productRowHolder.textView.setText("");
            }

            if(item.isLiked())
                productRowHolder.textView.setCompoundDrawablesWithIntrinsicBounds(0,0,R.mipmap.ic_like,0);
            else
                productRowHolder.textView.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0);


        }

        @Override
        public int getItemCount() {
            return (null != productList ? productList.size() : 0);
        }

        public class ProductRowHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            //Declaration of Views

            private ImageView imageView;
            private TextView textView;

            public ProductRowHolder(View view) {
                super(view);

                imageView = (ImageView) view.findViewById(R.id.imgArticle);
                textView = (TextView) view.findViewById(R.id.tvArticleTitle);


            }

            @Override
            public void onClick(View view) {
            }
        }
    }
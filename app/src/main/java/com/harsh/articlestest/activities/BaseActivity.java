package com.harsh.articlestest.activities;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.widget.Toast;

import com.harsh.articlestest.R;

/**
 * Created by Harshsoni on 13/10/15.
 */
public class BaseActivity extends AppCompatActivity {

    public void showToastCenter(String message){
        Toast toast = Toast.makeText(this,message, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    public Toolbar setToolBar(String title){
        Toolbar appbar = (Toolbar) findViewById(R.id.toolbar);
        appbar.setTitle("");
        appbar.setLogo(R.drawable.app_homelogo_small);
        setSupportActionBar(appbar);


        appbar.setNavigationIcon(R.mipmap.left_arrow);
            appbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });


        return appbar;
    }

}

package com.harsh.articlestest.utils;

import android.app.ProgressDialog;
import android.content.Context;

/**
 * Created by Harshsoni on 13/10/15.
 */
public class AppDialogs {

    private Context context;
    private ProgressDialog progressDialog;

    public AppDialogs(Context context){
        this.context = context;
    }
    public void showLoading(){
        progressDialog = ProgressDialog.show(context, "Loading", "Please wait...");
    }

    public void dismissLoading(){
        if(progressDialog !=null && progressDialog.isShowing()){
            progressDialog.dismiss();
        }
    }
}

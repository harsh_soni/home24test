package com.harsh.articlestest.VO;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Harshsoni on 13/10/15.
 */
public class MediaVO implements Parcelable {
    private String uri;
    private String mimeType;

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(getUri());
        dest.writeString(getMimeType());
    }

    public static final Parcelable.Creator<MediaVO> CREATOR = new Parcelable.Creator<MediaVO>() {
        public MediaVO[] newArray(int size) {
            return new MediaVO[size];
        }

        public MediaVO createFromParcel(Parcel in) {
            return new MediaVO(in);
        }
    };

    private MediaVO(Parcel in){
        setUri(in.readString());
        setMimeType(in.readString());
    }

}

package com.harsh.articlestest.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.harsh.articlestest.R;


public class FragmentLast extends Fragment {


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_last, container, false);
		return view;
	}

	@Override
	public void onStart() {
		super.onStart();
	}

}

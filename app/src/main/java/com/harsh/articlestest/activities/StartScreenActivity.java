package com.harsh.articlestest.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.harsh.articlestest.R;
import com.harsh.articlestest.VO.ArticlesVO;
import com.harsh.articlestest.VO.ResponseVO;
import com.harsh.articlestest.helper.RestClient;
import com.harsh.articlestest.utils.AppDialogs;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class StartScreenActivity extends BaseActivity {

    private final String ARTICLE_COUNT = "20";
    @Bind(R.id.btnStart) Button btnStart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_screen);
        ButterKnife.bind(this);

        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AppDialogs d = new AppDialogs(StartScreenActivity.this);
                d.showLoading();
                RestClient.get().getArticles(ARTICLE_COUNT , new Callback<ResponseVO>() {
                    @Override
                    public void success(ResponseVO responseVO, Response response) {
                        d.dismissLoading();
                        if(responseVO!=null){
                            if(responseVO.getArticlesCount()>0 && responseVO.getEmbeddedVO()!=null
                                    && responseVO.getEmbeddedVO().getArticles()!=null
                                    && !responseVO.getEmbeddedVO().getArticles().isEmpty()){

                                ArrayList<ArticlesVO> arr ;
                                if(responseVO.getEmbeddedVO().getArticles().size()>10){
                                    arr = new ArrayList<ArticlesVO>(responseVO.getEmbeddedVO().getArticles().subList(0,10));
                                }else{
                                    arr = responseVO.getEmbeddedVO().getArticles();
                                }
                                Intent i = new Intent(StartScreenActivity.this, ArticleSelectionActivity.class);
                                i.putParcelableArrayListExtra("articles", arr);
                                startActivity(i);
                            }else{
                                showToastCenter("No Articles found.");
                            }

                        }else{
                            d.dismissLoading();
                            showToastCenter(getResources().getString(R.string.error_msg));
                        }

                    }

                    @Override
                    public void failure(RetrofitError error) {
                        showToastCenter(getResources().getString(R.string.error_msg));

                    }
                });

            }
        });
    }

}

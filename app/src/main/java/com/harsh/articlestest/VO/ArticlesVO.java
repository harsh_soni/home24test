package com.harsh.articlestest.VO;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Harshsoni on 13/10/15.
 */
public class ArticlesVO implements Parcelable {

    private String title;
    private boolean isLiked = false;
    private ArrayList <MediaVO> media;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<MediaVO> getMedia() {
        return media;
    }

    public void setMedia(ArrayList<MediaVO> media) {
        this.media = media;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeString(getTitle());
        dest.writeList(getMedia());
        dest.writeByte((byte) (isLiked() ? 1 : 0));

    }

    public static final Parcelable.Creator<ArticlesVO> CREATOR = new Parcelable.Creator<ArticlesVO>() {
        public ArticlesVO[] newArray(int size) {
            return new ArticlesVO[size];
        }

        public ArticlesVO createFromParcel(Parcel in) {
            return new ArticlesVO(in);
        }
    };

    private ArticlesVO(Parcel in){

        setTitle(in.readString());
        media = new ArrayList<MediaVO>();
        media = in.readArrayList(MediaVO.class.getClassLoader());
        setIsLiked(in.readByte() != 0);

    }

    public boolean isLiked() {
        return isLiked;
    }

    public void setIsLiked(boolean isLiked) {
        this.isLiked = isLiked;
    }
}
